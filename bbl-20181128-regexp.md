---

# Regular Expressions
## A BBL at OCTO.ch

---

# Quizz - 1

I want to turn

    {Station: mockStations.Get("a"), LineId: "1"}
    {Station: mockStations.Get("b"), LineId: "1"}
    {Station: mockStations.Get("f"), LineId: "3"}
    {Station: mockStations.Get("g"), LineId: "3"}
    {Station: mockStations.Get("c"), LineId: "2"}
    ...

Into

    {Station: mockStations["a"], LineId: "1"}
    {Station: mockStations["b"], LineId: "1"}
    {Station: mockStations["f"], LineId: "3"}
    {Station: mockStations["g"], LineId: "3"}
    {Station: mockStations["c"], LineId: "2"}
    ...

---
# Quizz - 1

![](images/intellij-replace.png)

---
